import React, { useState }  from 'react';

import './styles/openweather.scss';
import { Container, Row, Col, Button } from 'react-bootstrap';

import Search from '../components/Search';
import WeatherByCity from '../components/WeatherByCity';
import { fetchWeather } from '../services';

const Openweather = () => {

  const [ form, setForm ] = useState({location: ''});

  const [ loading, setLoading ] = useState(false);
  const [ error, setError ] = useState({status: false, message: ''});
  const [ weather, setWeather ] = useState({data: ''})


  const handlerOnChange = ({target}) => {
    const { name, value } = target;

    setForm({
      ...form,
      [name]: value
    })
  }

  const handlerOnSubmit = event => {
    event.preventDefault();
    setLoading(true);
    fetchWeather(form.location).then(response => {
        // console.log('response: ', response.data)
        setWeather({data: response.data})
        setLoading(false);
    }, err => {
        // console.log('-------->', err)
        setError({
            error: true,
            message: err.response
        })
        // console.log(error)
        setLoading(false);
    })
  }

  return (
     <Container>
      <div className="main d-flex justify-content-center align-items-center">
        <div className="wrapper">
          <Search
              handlerOnSubmit={handlerOnSubmit} 
              handlerOnChange={handlerOnChange} 
              loading={loading}
              form={form} 
          />
          <WeatherByCity
              loading={loading}
              weather={weather}
          />
        </div>
      </div>
    </Container>
  )

}

export default Openweather;