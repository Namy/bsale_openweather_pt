import React from 'react';

import './styles/search.scss';
import { Container, Form, Row, Col, Button, Spinner } from 'react-bootstrap';

const Search = props => {

  return (
    <Form className="p-4" onSubmit={props.handlerOnSubmit}>
      <Form.Group>
        <div className="row mx-0">
          <div className="col-11 px-0">
            <Form.Control 
              type="text" 
              placeholder="Buscar Ciudad" 
              onChange={props.handlerOnChange}
              value={props.form.location}
              name="location"
              disabled={props.loading}
              bsPrefix="input-city text-center"
            />
          </div>
          <div className="col-1 pl-2">
            <Button 
              variant="primary" 
              type="submit" 
              disabled={props.loading}
              bsPrefix="btn-search"
            >
              {props.loading ? <Spinner animation="grow" variant="light" /> : <i class="fas fa-search"></i>}
            </Button>
          </div>
        </div>
      </Form.Group>            
    </Form>
  );
}

export default Search;