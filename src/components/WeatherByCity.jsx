import React from 'react';

import './styles/weather_icons.css';
import './styles/weatherbycity.scss';

import { Container } from 'react-bootstrap';

const IconsWeather = [
  { value: '01', icono: 'icon-sun2'},
  { value: '02', icono: 'icon-cloudy'},
  { value: '03', icono: 'icon-cloudy1'},
  { value: '04', icono: 'icon-cloudy3'},
  { value: '09', icono: 'icon-rainy1'},
  { value: '10', icono: 'icon-rainy2'},
  { value: '11', icono: 'icon-lightning'},
  { value: '13', icono: 'icon-snowy2'},
  { value: '50', icono: 'icon-weather'}
]

const WeatherByCity = props => {

  let newDate = new Date()
  let date = newDate.getDate();
  let month = newDate.getMonth() + 1;
  let year = newDate.getFullYear();
    
  // console.log(props.weather)

  var icon_api = props.weather.data.weather && props.weather.data.weather[0].icon.substring(0, 2);
  var icon_class = IconsWeather.find(item => item.value == icon_api);

  var icon_weather_api = props.weather.data.weather && props.weather.data.weather[0].icon + '.png';

  console.log(icon_weather_api);

  return (
    <div className="weather px-5">
      {
        props.loading ? 
        'Cargando...':

        <div className="row text-center pt-4">
          <div className="col-12">
            <div className="city-weather">
              <h1 className="title">{props.weather.data && props.weather.data.name}</h1>
              <small className="color-white-secondary">{date} / {month} / {year}</small>
            </div>
          </div>
          <div className="col-12 align-self-center py-3 px-0">
            <div className="image-weather">
              <img src={`http://openweathermap.org/img/wn/${icon_weather_api}`} className="icon-weather img-fluid" />
            </div>
          </div>
          <div className="col-12 pl-0 pb-3 align-self-center">
            <div className="temperature text-center">
              <span className="temp">{props.weather.data.main && props.weather.data.main.temp}</span> 
              <span className="units color-white-secondary">ºC</span>
            </div>
          </div>
          <div className="col-12">
            <div className="box py-3">
              <p className="description my-0">{props.weather.data.weather && props.weather.data.weather[0].description}</p>
              <span className="temp_min color-white-secondary">MIN. <b className="color-white">{props.weather.data.main && props.weather.data.main.temp_min}</b>ºC - </span>
              <span className="temp_max color-white-secondary"> MÁX. <b className="color-white">{props.weather.data.main && props.weather.data.main.temp_max}</b>ºC</span>
            </div>
          </div>
        </div>
      }
    </div>
  );
}

export default WeatherByCity;
