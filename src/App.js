import React from 'react';
import Openweather from './pages/openweather.jsx';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return <Openweather />;
}

export default App;